@extends('layouts.default')

@section('content')
  This is the home page, it is not used in application testing.
  <br>
  The only parts with usage are the register, login, browsing, admin dashboard and profile areas.
  <br>
  You could have some 'recommended catalogues' or 'popular catalogs in rows' here.
@endsection