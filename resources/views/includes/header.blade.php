<!DOCTYPE html>
<div class="header-upper">
  <a id="logo" href="/">Title Logo</a>
  <div class="user-profile" style="float: right; clear: both;">
    @if(isset($_SESSION['token']))
    @if(strcasecmp( $_SESSION['user']['scope'] , 'admin' ) == 0)
    <a href="/dashboard"> Admin dashboard</a>
    @endif
    <a href="/profile"> {{$_SESSION['user']['first_name']}}'s profile</a>
    <a href="/logout">Logout</a>
    @else
    <a href="/register">Register</a>
    <a href="/login">Log in</a>
    @endif
  </div>
</div>
<div class="header-lower">
  <ul class="nav" style="float: right; clear: both;">
    <li><a href="/">Home</a></li>
    <li><a href="/browse">Browse</a></li>
    <li><a href="/about">About</a></li>
    <li><a href="/contact">Contact</a></li>
  </ul>
</div>
