<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  @include('includes.head')
</head>
<body>
  <div class='container'>
    <header>
      @include('includes.header')
    </header>
    <div id="main" style="padding-top: 140px;">
      @yield('content')
    </div>
    <footer>
      @include('includes.footer')
    </footer>
  </div>
</body>
</html>