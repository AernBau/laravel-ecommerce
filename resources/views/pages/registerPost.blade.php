@extends('layouts.default')

@section('content')
  <!-- Create a form, absolute position, width=x, margin-right=x/2, top: 50%, left: 50% -->
  <style>
    #main {
      position: absolute;
      left: 50%;
      top: 35%;
      width: 400px;
    }
    
  </style>
  <div style="position: absolute; left: -75%;">
  <?php 
    /*echo $reg_info['username'];
    echo $reg_info['first_name'];
    echo $reg_info['last_name'];
    echo $reg_info['email'];*/
    echo "User has been registered. Please log in.";
    DB::table('users')->insert(
      [
        'username' => $reg_info['username'],
        'first_name' => $reg_info['first_name'],
        'last_name' => $reg_info['last_name'],
        'email' => $reg_info['email'],
        'password' => $reg_info['password'],
        'role' => 'user',
        'created_at' => DB::raw('NOW()'),
        'updated_at' => DB::raw('NOW()'),
      ]
    );
  ?>
  </div>
@endsection