@extends('layouts.default')
  <style>
    #main {
      position: absolute;
      left: 50%;
      top: 30%;
      width: 400px;
    }
    
    form {
      width: inherit;
      position: absolute;
      left: -50%;
      top: 0%;
    }
    
    form label, input{
      height: 40px;
    }
    
    form label {
      width: 125px;
      display: inline-block;
    }
    
    form input {
      width: inherit;
      background-color: white;
      border-style: groove;
      padding: 0px;
      font-family: "Open Sans";
      font-size: 16px;
      padding: 5px 10px;
      -webkit-box-sizing: border-box; /* Safari/Chrome, other WebKit */
      -moz-box-sizing: border-box;    /* Firefox, other Gecko */
      box-sizing: border-box;         /* Opera/IE 8+ */
    }
    
  </style>
@section('content')
  {!! Form::open(['url' => 'login']) !!}
    {!! Form::text('username','',['required', 'placeholder' => 'Username']) !!}
    <br>
    {!! Form::text('password','',['required', 'placeholder' => 'Password']) !!}
    <br>
    {!! Form::submit('Login') !!}
  {!! Form::close() !!}
</div>
  <div id="errors" ><?php 
    foreach ($errors->all() as $message) {
      echo "<div class='error'>";
      echo $message;
      echo "</div>";
    }
  ?>
@endsection