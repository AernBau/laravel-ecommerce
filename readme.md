# Laravel e-commerce project

This project is made as a part of Hivelocity JP's Recruitment Test.  
The web application is an e-commerce type application that allows users to purchase items.

## Stage plan

Stage 1: Database planning and creation. COMPLETE.  
Stage 2: Basic website frame and registration. COMPLETE.  
Stage 3: Authorization. Basic: COMPLETE. OAuth: On hold.  
Stage 4: Admin and user interface. Basic: COMPLETE.  Complete: COMPLETE.
Stage 5: Catalog and items.  Basic: COMPLETE. AWS: COMPLETE. PDF generation: COMPLETE;
Stage 6: Deployment and mail system. In progress.
