@extends('layouts.default')
  
@section('content')
  <style>
    #browse_area {
      position: absolute;
      top: 20%;
      width: 100%;
      height: 75%;
      display: inline-flex;
      display: -webkit-box;
      overflow-x: auto
    }
    
    .cat_item {
      margin: 2%;
      height: 300px;
      background-color: #BBB;
      border-radius: 5px;
    }
    
    .cat_item a {
      color: inherit;
      text-decoration: none;  
    }
    
    .cat_title {
      font-size: 24px;
      margin: 15px;
      margin-bottom: 5px;
    }
    
    .cat_descr {
      font-size: 16px;
      margin: 0px 15px;
    }
    
    .cat_img{
      height: 200px;
      margin: 0px 15px;
      background-color: #AEAEAE;
    }
    
    .cat_button {
      height: 30px;
      width: 100%;
      font-family: "Lato";
      background-color: #CCC;
      border-style: groove;
      border-radius: 5px;
      outline: none;
    }
  </style>
  <div id='browse_area'>
    <?php 
    foreach ($catalogues as $catalogue) {  
      $purchase_bool = false;
      echo "<div class='cat_item' id='catalogue".$catalogue->id."'>";
        if(isset($purchases)){
          foreach ($purchases as $user_purchase){
            if($user_purchase->cat_id == $catalogue->id){
              $purchase_bool = true;
            }
          }
        }
        if(!$purchase_bool){
          echo "<form id='form".$catalogue->id."' method='POST' action='purchases'>";
        } 
          echo "<a href='browse/".$catalogue->id."'>".
          "<div class='cat_title'>".$catalogue->title."</div><div class='cat_descr'>".$catalogue->description."</div>".
          "<input hidden readonly name='cat_id' value='".$catalogue->id."'></input>".
          "<div>";
        if(isset($items[$catalogue->id])){
          echo "<img class='cat_img' src='/image/".$items[$catalogue->id]->id."' alt='".$catalogue->title."'></img>";
        } else {
          echo "<img class='cat_img' alt='".$catalogue->title."'></img>";
        }
          echo "</div></a>";
        if(!$purchase_bool){
          echo "<input type='submit' class='cat_button' value='Buy for ".$catalogue->cost."'></input></form>";
        } else {
          echo "<button class='cat_button' type='button' value='Already purchased' onclick='location.href=\"/browse/".$catalogue->id."\"'>Already purchased</button>";
        }
      echo"</div>";
    }
    
    ?>
  </div>
@endsection