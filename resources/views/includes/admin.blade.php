@extends('layouts.default')
<div id="dashboard">
  <div id="sidebar">
    <div id="greet">
      Hello,  {{$_SESSION['user']['first_name']}}  
    </div>
     <div id="sidemenu">
      <ul>
        <?php
          $array = ['Dashboard','Users','Catalogues','Items','Purchases'];
          foreach($array as $item)
            echo "<button id='".$item."'>".$item."</button>";
        ?>
      </ul>
    </div>
  </div>
  <div id="dashcontent">
    @yield('dashcontent')
  </div>
</div>
