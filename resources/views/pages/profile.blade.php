@extends('layouts.default')

<style>
   #browse_area {
      position: absolute;
      top: 20%;
      width: 100%;
      height: 75%;
      display: -webkit-box;
      overflow-x: auto
  }
    
    
  #greetP {
    font-size: 2.5vw;
    text-align: center;
    color: #444;
    transition: all 0.3s ease 0s;
    position: absolute;
    top: 10%;
    left: 5%;
    padding-top: 120px;
  }
  
  #purchases {
    font-size: 2vw;
    color: #444;
    transition: all 0.3s ease 0s;
    position: absolute;
    top: 10%;
    left: 25%;
    padding-top: 90px;
  }
  
  #purchases a {
    color: inherit;
    text-decoration: none;
  }
  
  #purchases > a {
    display: table-cell;
    padding-top: 15px;
    padding-right: 20px;
  }
  
  #purchases #description {
    font-size: 1.5vw;
  }
  
  #purchases a img {
    height: 240px;
  }
  
  #purchases > a > a {
    z-index: 600;
    transition: all 0.3s ease 0s;
  }
  
  #purchases > a > a:hover {
    background-color: #BBB;
    color: #222;
  }
  
  
</style>
<div id='browse_area'>
</div>
  
<script type = "text/babel">
  
  var Greeting = React.createClass({
    render: function() {
      return (
        <div id="greetP">
          Hello, {{$_SESSION['user']['first_name']}}
        </div>
      );
    }
  });
  
  var Purchases = React.createClass({
    render: function() {
      <?php echo "var purchases = ". json_encode($purchases) . ";\n"; ?>
      <?php echo "var items = ". json_encode($items) . ";\n"; ?>
      var func = purchases.map(function(purchase){
        var link = 'browse/'+purchase['id'];
        var image_link = "/image/"+items[purchase['id']]['id'];
        var download_link = 'browse/'+purchase['id']+"/download";
        return ([
          <a href={link}>
            <div id='title'>{purchase['title']}</div>
            <img src={image_link}></img>
            <div id='description'>{purchase['description']}</div>
            <a href={download_link} class='link'>Download PDF here</a>
          </a>]);
      });
      return (
        <div id="purchases">
          Purchased catalogues:
          {func}
        </div>
      );
    }
  });

  ReactDOM.render(
    <div>
      <Greeting />
      <Purchases />
    </div>,
    document.getElementById('main')

  );

</script>
