<?php

namespace App\Http\Controllers;

use Auth;
use Hash; 
use Validator;
use Storage;
use PDF;
use AWS;
use Aws\CommandPool;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\MessageBag;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use League\Flysystem\AwsS3v3\AwsS3Adapter;
use League\Flysystem\Filesystem;

class UserController extends Controller
{
 
  public function index()
  {
    return view('pages.home');
  }
  
  public function register()
  {
    return view('pages.registerGet');
  }
  
  public function login()
  {
    return view('pages.login');
  }
   
  public function about()
  {
    return view('pages.about');
  }
  
  public function contact()
  {
    return view('pages.contact');
  }
  
  public function browse()
  {
    $catalogues = DB::table('catalogues')->get();
    
    
    $items = [];
    foreach($catalogues as $catalogue){
      $items[$catalogue->id] = DB::table('items')->where('cat_id',$catalogue->id)->first();    
    }
    $data = [
      'catalogues' => $catalogues,
      'items'      => $items,
    ];
    if(isset($_SESSION['token'])){
      $purchases = DB::table('purchases')->where('user_id',$_SESSION['user']['user_id'])->get();    
      $data['purchases'] = $purchases;
    } 
    return view('pages.browse', $data);
  }
  
  public function profile()
  {
    $purchases = DB::table('purchases')->where('user_id',$_SESSION['user']['user_id'])->join('catalogues', 'purchases.cat_id', '=', 'catalogues.id')->get();
    $items = [];
    foreach($purchases as $purchase){
      $items[$purchase->id] = DB::table('items')->where('cat_id', $purchase->cat_id)->first();
    }
    return view('pages.profile', ['purchases' => $purchases, 'items' => $items]);
  }
  
  /**
   * Creates a new user.
   *
   * @return void
   */
  public function create(Request $request)
  {
    $validator = Validator::make($request->all(), [
      'username'  => 'required|min:3|unique:users,username|max:30',
      'password'  => 'required|min:4|max:60 ',
      'email'     => 'required|email|unique:users,email|max:60'
    ]);

    $request->flashExcept('password', 'email');
    $reg_info = $request->all();

    if ($validator->fails())
    {
      return redirect('register')
        ->withErrors($validator)
        ->withInput();
    }
    else {
      $reg_info['password'] = Hash::make($reg_info['password']);
      return view('pages.registerPost', ['reg_info' => $reg_info]);
    }
  }
  
  /**
    * Returns all catalogues in the system (JSON).
    */
  
  public function getCatalogues(Request $request)
  {
    $catalogues = DB::table('catalogues')->get();
    return response()->json($catalogues);
  }
  
  /**
    * Displays a catalogue (visual);
    */
  public function showCatalogue($catalogue_id)
  {
    $catalogue = DB::table('catalogues')->where('id',$catalogue_id)->first();
    $items = DB::table('items')->where('cat_id',$catalogue_id)->get(); 
    $out_array = [
      'cat_data'  => $catalogue,
      'item_data' => $items,
    ];
    if(isset($_SESSION['user'])){
      $count = DB::table('purchases')->where(['cat_id' => $catalogue_id, 'user_id' => $_SESSION['user']['user_id']])->count();
      if($count){
        $out_array['purchases'] = $count;
      }
    }
    return view('pages.catalogue', $out_array);
  }

  public function newPurchase(Request $request)
  {
    $catalog = DB::table('catalogues')->where(['id' => $request['cat_id']])->get();
    $request['user_id'] = $_SESSION['user']['user_id'];
    $validator = Validator::make($request->all(), [
      'user_id'  => 'required|exists:users,id|unique:purchases,user_id,NULL,id,cat_id,' . $request['cat_id'],
      'cat_id'   => 'required|exists:catalogues,id|unique:purchases,cat_id,NULL,id,user_id,' . $request['user_id'],
    ]);

    if ($validator->fails())
    {
      return redirect('browse')
        ->withErrors($validator)
        ->withInput();  
    }
    else {
      DB::table('purchases')->insert(
        [
          'user_id'    => $request['user_id'],
          'cat_id'     => $request['cat_id'],
          'created_at' => DB::raw('NOW()'),
          'updated_at' => DB::raw('NOW()'),
        ]
      );
      return redirect('browse/'.$request['cat_id']);
    }
    
  }
  
  public function getImage($image_id)
  {
    
    if(!isset($_SESSION['user']['user_id'])){
      // this should never hapen, but if it does, break;
      return false;
    }
    else {
      $user_id = $_SESSION['user']['user_id'];
    }
    
    $purchased = DB::table('purchases')
      ->join('catalogues', 'purchases.cat_id', '=', 'catalogues.id')
      ->join('items', 'catalogues.id', '=', 'items.cat_id')
      ->where(['items.id' => $image_id])->where(['user_id' => $user_id])->count();  
    
    $item = DB::table('items')->where(['id' => $image_id])->first();
    $s3 = AWS::createClient('s3');
    $image = $s3->getObject(array(
        'Bucket'     => 'laravel-test-ecommerce',
        'Key'        => $item->location,
    )); 
    
    header("Content-type: image/jpeg");
    $img = imagecreatefromstring($image->get('Body'));
    if(!$purchased){
      $img = imagescale($img, 200);
    }
    return imagejpeg($img);
  }
  
  public function downloadCatalogue($download_id)
  {
    $is_purchased = DB::table('purchases')->where(['cat_id' => $download_id, 'user_id' => $_SESSION['user']['user_id']])->count();
    
    $s3 = AWS::createClient('s3');

    if($is_purchased){
      $items = DB::table('items')->where(['cat_id' => $download_id])->get();
      PDF::SetAutoPageBreak(false, 0);
      foreach($items as $item){
        
        $opts = array('http' => array('header'=> 'Cookie: ' . $_SERVER['HTTP_COOKIE']."\r\n"));
        $context = stream_context_create($opts);
        
        // Closing and starting the session is faster than to keep it running.
        session_write_close();
        $image_n = file_get_contents("http://".$_SERVER['HTTP_HOST']."/image/".$item->id, false, $context);
        session_start();
        
        PDF::AddPage("L");
        //PDF::Image(file, offsetx, offsety, width, 297, '', '', '', false, 300, '',  false, false, 0);
        PDF::Image('@'.$image_n, 0, 0, 0, 0, '', '', '', false, 300, '', false, false, 0, true, false, false);
        PDF::SetPageMark();
      }
      return PDF::Output('catalogue.pdf');
    } else {
      return redirect('browse')
          ->withErrors(['error' => "You have not purchased the gallery."]);
    }
  }
  
}

?>