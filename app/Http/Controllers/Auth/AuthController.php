<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Validator;
use Auth; use Authorizer;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class AuthController extends Controller
{
    use AuthenticatesAndRegistersUsers, ThrottlesLogins;
  
    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware($this->guestMiddleware(), ['except' => ['logout']]);
    }

    public function getIndex()
    {
        return redirect('/');
    }
  
    public function postLogin($username, $password)
    { 
        return Auth::user()->username;
    }
  
    public function logout()
    {
        session_unset();
        return redirect('/');
    }
  
}
