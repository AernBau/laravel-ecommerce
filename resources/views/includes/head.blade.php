<!DOCTYPE html>
<meta charset="utf-8">

<title>E-Commerce Application</title>

<link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
<script src="https://unpkg.com/react@15.3.2/dist/react.js"></script>
<script src="https://unpkg.com/react-dom@15.3.2/dist/react-dom.js"></script>
<script src="https://unpkg.com/babel-core@5.8.38/browser.min.js"></script>
<script src="https://unpkg.com/jquery@3.1.0/dist/jquery.min.js"></script>
<script src="https://unpkg.com/remarkable@1.6.2/dist/remarkable.min.js"></script>
<style>
  html {
    min-width: 840px;
    min-height: 600px;
    background-color: #DDD;
    overflow: hidden;
  }
  
  body {
    margin: 0px;
    font-family: "Lato";
  }
  
  header, footer{
    background-color: #333;
    width: 100%;
    min-width: 520px;
    z-index: 9000;
  }
  
  header {
    position: absolute;
    height: 20%;
    min-height: 120px
  }
  
  .user-profile {
    padding: 10px; 
  }
  
  header ul {
    list-style-type: none;
    margin: 0;
    padding: 0;
    overflow: hidden;
  }
  
  header li {
    float: left;
  }

  header #logo{
    font-size: 4vw;
    padding: 10px 20px;
  }
  
  header a, #logo, footer{
    display: inline-block;
    text-align: center;
    transition: all 0.3s ease 0s;
    padding: 10px;
    color: #DDD;
    text-decoration: none;
  }
  
  header li a:hover {
    background-color: #111;
  }
  
  footer {
    position: absolute;
    bottom: 0;
    padding: 0;
    height: 5%;
    min-height: 30px;
  }
  
  #copyright {
    float: right;
    padding: 0.5% 20px;
  }
  
  #errors {
    color: #D44; 
    position: absolute;
    top:  50%;
    right: -15%;
    width:  400px;
    transform: translate(-50%, -50%);
    pointer-events: none;
  }

  #errors .error {
    padding: 10px;
    pointer-events: none;
  }

  #sidebar {
    background-color: #BBB;
    position: absolute;
    top: 20%;
    width: 20%;
    height: 75%;
  }
 
  #greet {
    float: left;
    clear: both;
    line-height: 3;
    font-size: 2.5vw;
    text-align: center;
    width: 100%;
    border-style: groove;
    border-top: 0;
    border-left: 0;
    border-right: 0;
    border-color: #BBB;
    color: #444;
    transition: all 0.3s ease 0s;
  }
  
  #sidebar ul {
    list-style: none;
    padding: 0;
    margin: 0;
    display: table;
    width: 100%;
    color: #CCC;
  }
   
  #sidemenu {
    height: 100%;  
  }
  
  #sidebar ul button {
    padding: 5% 0px;
    text-align: center;
    width: 100%;
    outline: none;
    background-color: transparent;
    border-left: 0;
    border-right: 0;
    border-style: none;
    transition: all 0.3s ease 0s;
    color: #555;
    font-size: 1.5vw;
    font-family: "Lato";
  }
  
  #sidebar ul button:nth-child(odd) {
    background-color: #CCC;
  }
  
  #sidebar ul button:hover {
    background-color: #EEE;
    color: #222;
  }
  
  #sidebar ul button:first-child {
    border-top: 0;
  }
  
  #sidebar ul button:last-child {
    border-bottom: 0;
  }
  
  #dashcontent {
    background-color: #DDD;
    position: absolute;
    top: 20%;
    width: 80%;
    height: 75%;
    right: 0;
  }
    
</style>