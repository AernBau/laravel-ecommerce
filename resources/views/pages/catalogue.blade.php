@extends('layouts.default')

@section('content')
  <style>
    #catalog_area {
      position: absolute;
      top: 20%;
      width: 100%;
      padding-top: 10px;
      display: inline-flex;
      display: -webkit-box;
      overflow-x: auto;
    }
    
    #cat {
      padding: 2%;
      max-width: 300px;
    }
    
    #cat_title {
      font-size: 2.5vw;
      color: #444;
      transition: all 0.3s ease 0s;
      top: 10%;
      left: 5%;
      padding-bottom: 25px  ;
    }
    
    #cat_description {
      font-size: 1.5vw; 
    }
    
    #cat_cost {
      font-size: 2vw;
    }
    
    #items {
      overflow-x: auto;
      overflow-y: hidden;
      display: inline-flex;
      padding: 10px;  
      height: 470px;
    }
    
    .item {
      height: 66px;
      font-size: 2vw;
    }
    
    #empty {
      width: 600px; 
      padding: 5%; 
      font-size: 3vw;
    }
    
    #item {
      margin: 10px; 
      width: 400px;
    }
    
    #item img {
      width: 100%;
    }
    
    #cat a {
      color: #444;
      z-index: 600;
      transition: all 0.3s ease 0s;
      text-decoration: none;
    }

    #cat a:hover {
      background-color: #BBB;
      color: #222;
    }
    
    form input {
      background-color: transparent;
      border-style: none;
      padding: 0;
      outline: none;
      font-size: 22px;
      font-family: "Lato";
    }
    
    form input:hover {
      background-color: #BBB;
    }
    
  </style>
  <div id="catalog_area">
     <?php 
        echo "<div id='cat'>";
        echo "<div id='cat_title'>".$cat_data->title."</div>";
        echo "<div id='cat_description'>".$cat_data->description."</div>";
        if(isset($purchases)){
          echo "<form id='downloadForm' method='get' action='/browse/".$cat_data->id."/download'>";
          echo "<input type='submit' class='download_button' value='Purchased | Download PDF here'></input>";
          echo "</form>";
        } else {
          echo "<div id='cat_description'> Purchase to remove the resolution limit </div>";
          //echo "<div id='cat_cost'> Price = ".$cat_data->cost."</div>";
          echo "<form id='form".$cat_data->id."' method='POST' action='/purchases'>".
          "<input hidden readonly name='cat_id' value='".$cat_data->id."'></input>".
          "<input type='submit' class='cat_button' value='Buy for ".$cat_data->cost." here'></input></form>";
        }
        echo "</div>";

        echo "<div id='items'>";
        foreach($item_data as $item){
          echo "<div id='item'>";
          echo "<div class='item'>".$item->title."</div>";
          echo "<div id='img_holder' style='width: 100%;'>";
          echo "<img src='/image/".$item->id."'></img>";
          echo "</div>";
          echo "<div>".$item->description."</div>";
          echo "</div>";
        }
        if (empty($item_data)) {
           echo "<div id='empty' >This catalogue is empty</div>";
        }
        echo "</div>";
    ?>
  </div>
@endsection