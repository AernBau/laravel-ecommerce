@extends('includes.admin')
<style>
  
  #dashcontent {
    display: inline-flex;
    display: -webkit-box;
    overflow-x: scroll;
  }
  
  #forms {
    width: 360px;
    padding: 2%;
    background-color: #AAA;
    font-size: 20px;
    text-align: left;
  }
  
  form {
    width: inherit;
    padding-top: 5%;
    margin: 0;
  }

  form label, input{
    font-size: 12px;
  }
  
  form select { 
    font-family: "Open Sans";
    background-color: #EEE;
    height: 30px;
  }

  form label, form input, form select {
    width: 178px;
  }

  form label {
    display: inline-block;
  }

  form input {
    background-color: #EEE;
    border-style: groove;
    padding: 0px;
    font-family: "Open Sans";
    padding: 5px 10px;
    -webkit-box-sizing: border-box; /* Safari/Chrome, other WebKit */
    -moz-box-sizing: border-box;    /* Firefox, other Gecko */
    box-sizing: border-box;         /* Opera/IE 8+ */
    outline: none;
  }
  
</style>
@section('dashcontent')

    <div id="forms">Create Catalogue
      {!! Form::open(['method' => 'post', 'url' => 'cat' ]) !!}
        <?php
          $array = ['title','description','cost'];
          foreach($array as $item){
            $label = "Create ".$item;
            echo "<label for='".$item."'>".$label."</label>";
            echo "<input required='required' name='".$item."' type='text' id='".$item."' />";
            echo "<br/>";
          }
        ?>
        <input type="reset" value="Reset fields"/>
        <input type="submit" value="Create Catalogue"/>
      {!! Form::close() !!}
      <br>
      Create Item
      <form id="postItemForm" method="post" action="cat/item" enctype="multipart/form-data">
        <label for="title">Title</label>
        <input required="required" name="title" type="text" id="title" />
        <label for="description">Description</label>
        <input required="required" name="description" type="text" id="description" />
        <label for="cat_id">Catalogue name</label>
        <select name="cat_id" id="cat_id">
        <?php 
          foreach($catalogues as $catalogue){
          echo "<option value=".$catalogue->id.">".$catalogue->title."</option>";
          }
        ?>
        </select>
        <label for="file">Image file</label>
        <input type="file" name="image" accept="image/*" />
        <label for="status">Status</label>
        <input required="required" name="status" type="text" id="status" />
        <input type="reset" value="Reset fields" />
        <input type="submit" value="Create Item" />
      </form>
    </div>
    <div id="forms">
      Update Catalogue
      {!! Form::open(['method' => 'put', 'url' => 'cat' ]) !!}
        <label for="cat_id">Catalogue</label>
        <select name="cat_id" id="cat_id">
        <?php 
        foreach($catalogues as $catalogue){
        echo "<option value=".$catalogue->id.">".$catalogue->title."</option>";
        }
        ?>
        </select>
        <label for="title">Title</label>
        <input required="required" name="title" type="text" id="title" />
        <label for="submit"></label>
        <input type="submit" value="Update Catalogue"/>
      {!! Form::close() !!}
      <br>
      Update Item Location
      {!! Form::open(['method' => 'put', 'url' => 'cat/item' ]) !!}
        <label for="item_id">Item name</label>
        <select name="item_id" id="cat_id">
        <?php 
          foreach($items as $item){
          echo "<option value=".$item->id.">".$item->title."</option>";
          }
        ?>
        </select>
        <label for="cat_id">Catalogue name</label>
        <select name="cat_id" id="cat_id">
        <?php 
          foreach($catalogues as $catalogue){
          echo "<option value=".$catalogue->id.">".$catalogue->title."</option>";
          }
        ?>
        </select>
        <label for="submit"></label>
        <input type="submit" value="Update Item"/>
      {!! Form::close() !!}
    </div>
    <div id="forms">
      Delete Catalogue
      {!! Form::open(['method' => 'delete', 'url' => 'cat' ]) !!}
        <label for="cat_id">Catalogue</label>
        <select name="cat_id" id="cat_id">
        <?php 
        foreach($catalogues as $catalogue){
        echo "<option value=".$catalogue->id.">".$catalogue->title."</option>";
        }
        ?>
        </select>
        <label for="submit"></label>
        <input type="submit" value="Delete Catalogue"/>
      {!! Form::close() !!}
      <br>
      Delete Item
      {!! Form::open(['method' => 'delete', 'url' => 'cat/item' ]) !!}
        <label for="item_id">Catalogue</label>
        <select name="item_id" id="cat_id">
        <?php 
          foreach($items as $item){
          echo "<option value=".$item->id.">".$item->title."</option>";
          }
        ?>
        </select>
        <label for="submit"></label>
        <input type="submit" value="Delete Item"/>
      {!! Form::close() !!}
      
    </div>
    <div id="errors" ><?php 
      foreach ($errors->all() as $message) {
        echo "<div class='error'>";
        echo $message;
        echo "</div>";
      }
    ?>
    </div>
  </div>
@endsection