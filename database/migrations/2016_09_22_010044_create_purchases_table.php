<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchasesTable extends Migration
{
    public $increments = false;
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchases', function (Blueprint $table) {
            $table->integer('user_id')->unsigned()->onDelete('cascade');
            $table->integer('cat_id')->unsigned()->onDelete('cascade');
            $table->primary(array('user_id', 'cat_id'));
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('cat_id')->references('id')->on('catalogues');
            $table->timestamps();
        });
        //DB::unprepared("ALTER TABLE 'purchases' DROP PRIMARY KEY, ADD PRIMARY KEY ('user_id','cat_id')");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('purchases');
    }
}
