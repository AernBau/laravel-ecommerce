<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Authenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
      /*
        if (Auth::guard($guard)->guest()) {
            if ($request->ajax() || $request->wantsJson()) {
                return response('Unauthorized.', 401);
            } else {
                return redirect()->guest('login');
            }
        }
        */
      /*
        Response::header('Cache-Control', 'nocache, no-store, max-age=0, must-revalidate');
        Response::header('Pragma', 'no-cache');
        Response::header('Authorization', 'Bearer '.$_SESSION['access_token']['access_token']);
        Response::header('Expires', 'Fri, 01 Jan 1990 00:00:00 GMT');
      */
        /*
        response()->header('Cache-Control', 'nocache, no-store, max-age=0, must-revalidate');
        response()->header('Pragma', 'no-cache');
        response()->header('Authorization', 'Bearer '.$_SESSION['access_token']['access_token']);
        response()->header('Expires', 'Fri, 01 Jan 1990 00:00:00 GMT');
      */
        if(isset($_SESSION['token'])){
          return $next($request)->header('Authorization', 'Bearer '.$_SESSION['token']['access_token']);
        } else {
          return redirect('login');
        }
    }
}
