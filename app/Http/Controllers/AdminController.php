<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use AWS;
use App\Http\Requests;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Validator;
use Auth; 
use Storage;

class AdminController extends Controller
{
    //
    public function dashboard()
    {
      
        if(strcasecmp( $_SESSION['user']['scope'] , 'admin' ) != 0)
        {
            return redirect('/');
        }
        $catalogues = DB::table('catalogues')->get();
        $items = DB::table('items')->get();
        return view('pages.dashboard', ['catalogues' => $catalogues, 'items' => $items]);
    }
  
    public function newCatalogue(Request $request)
    {
      $validator = Validator::make($request->all(), [
        'title' => 'required|min:3|unique:catalogues,title|max:50',
        'description' => 'required|min:4|max:600 ',
        'cost' => 'required|max:10'
      ]);

      $reg_info = $request->all();

      if ($validator->fails())
      {
        return redirect('dashboard')
          ->withErrors($validator)
          ->withInput();
      }
      else {
        DB::table('catalogues')->insert(
          [
            'title' => $reg_info['title'],
            'description' => $reg_info['description'],
            'cost' => $reg_info['cost'],
            'created_at' => DB::raw('NOW()'),
            'updated_at' => DB::raw('NOW()'),
          ]
        );
        return redirect('dashboard');
      }
    }
  
    public function updateCatalogue(Request $request)
    {
      DB::table('catalogues')->where('id', '=', $request->cat_id)->update(['title' => $request->title]);
      return redirect('dashboard');
    }
  
    public function deleteCatalogue(Request $request)
    {
      DB::table('catalogues')->where('id', '=', $request->cat_id)->delete();
      return redirect('dashboard');
    }

    public function getPurchases(Request $request)
    {
      if (!($request['user_id']))
      {
        $purchase = DB::table('purchases')->get();
      } else {
        $purchase = DB::table('purchases')->where('user_id',$request['user_id'])->get();
      }
      return $purchase;
    }
  
    public function newItem(Request $request)
    {
      $catalogue = DB::table('catalogues')->where(['id' => $request['cat_id']])->first();
      //$file = $request;
      // Puts a text file with string 'contents' in it.
      //Storage::disk('local')->put('file.txt', 'Contents');
      // Attempts to put in a useless string into a jpg.
      //Storage::disk('local')->put('file.jpg', $file);
      //return var_dump($file);
      
      $validator = Validator::make($request->all(), [
        'title' => 'required|min:3|unique:items,title,NULL,id,cat_id,'.$request['cat_id'].'|max:50',
        'cat_id' => 'required|max:30|unique:items,cat_id,NULL,id,title,' . $request['title'],
      ]);
      
      $file = $request['image'];
      list($width, $height) = getimagesize($file);
      $resolution = $width."x".$height;
      //$disk = Storage::disk('s3');
      //Storage::put('file.jpg', $file);
      $s3 = AWS::createClient('s3');
      
      $reg_info = $request->all();
      $filename = $file->getClientOriginalName();
      $location = 'images/'.$filename;
      if ($validator->fails())
      {
        return redirect('dashboard')
          ->withErrors($validator)
          ->withInput();  
      }
      else {
        $result = $s3->putObject(array(
            'Bucket'     => 'laravel-test-ecommerce',
            'Key'        => $location,
            'SourceFile' => $file->getPathname(),
        ));
        DB::table('items')->insert(
          [
            'title' => $reg_info['title'],
            'cat_id' => $catalogue->id,
            'location' => $location,
            'size' => $file->getClientSize(),
            'resolution' => $resolution,
            'description' => $reg_info['description'],
            'status' => $reg_info['status'],
            'created_at' => DB::raw('NOW()'),
            'updated_at' => DB::raw('NOW()'),
          ]
        );
        return redirect('/dashboard');
      }
    }
    
    public function deleteItem(Request $request){
        DB::table('items')->where('id', '=', $request->item_id)->delete();
        return redirect('dashboard');
    }
  
    public function updateItem(Request $request){
        DB::table('items')->where('id', '=', $request->item_id)->update(['cat_id' => $request->cat_id]);
        return redirect('dashboard');
    }
} 
