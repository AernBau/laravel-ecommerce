<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/**
 * OAuth
 */

use Illuminate\Http\Request;

session_start();

Route::group(['middleware' => 'auth'], function () {
    Route::get('profile', 'UserController@profile');
    Route::get('dashboard', 'AdminController@dashboard');
    
    Route::post('cat', 'AdminController@newCatalogue');
    Route::delete('cat', 'AdminController@deleteCatalogue');
    Route::put('cat', 'AdminController@updateCatalogue');
  
    Route::post('cat/item', 'AdminController@newItem');
    Route::delete('cat/item', 'AdminController@deleteItem');
    Route::put('cat/item', 'AdminController@updateItem');
  
    Route::get('purchases', 'AdminController@getPurchases');
    Route::post('purchases', 'UserController@newPurchase');
    
    Route::get('browse/{cat_id}/download', 'UserController@downloadCatalogue');
    Route::post('image/{image_id}', 'UserController@getForDownload');
});

Route::group(['middleware' => 'guest'], function () {
    Route::get('register', 'UserController@register');
    Route::post('register', 'UserController@create');
    Route::get('login', 'UserController@login');
    Route::post('login', function(Request $request) {
      // Due to the oauth library used, I have to use this kind of function.
      // Could not figure out how to do it without reworking the library itself.
      $request['grant_type'] = "password";
      $request['client_id'] = "oauth";
      $request['client_secret'] = "secret";
      $credentials = array(
          'username' => $request['username'],
          'password' => $request['password']
      );
      if(Auth::once($credentials)) {
          $_SESSION['user']['username'] = Auth::user()->username;
          $_SESSION['user']['first_name'] = Auth::user()->first_name;
          $_SESSION['user']['last_name'] = Auth::user()->last_name;
          $_SESSION['user']['user_id'] = Auth::user()->id;
          $_SESSION['user']['email'] = Auth::user()->email;
          $_SESSION['user']['scope'] = Auth::user()->role;
          $_SESSION['token'] = Authorizer::issueAccessToken();
          return redirect('/');
      } else {
          $error = "User credentials incorrect.";
          return redirect('/login')
                  ->withErrors($error)
                  ->withInput();
      }
    });
});


Route::get('/', 'UserController@index');
Route::get('logout', 'Auth\AuthController@logout');

Route::get('cat', 'UserController@getCatalogues');

Route::get('browse', 'UserController@browse');
Route::get('browse/{cat_id}', 'UserController@showCatalogue');
Route::get('image/{image_id}', 'UserController@getImage');

Route::get('about', 'UserController@about');
Route::get('contact', 'UserController@contact');    

?>